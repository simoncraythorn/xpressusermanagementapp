﻿using System.Net.Http;

namespace XPressUserManagementApp
{
    public class ApiBadRequestException : ApiException
    {
        public ApiBadRequestException(HttpResponseMessage response)
            : base(response)
        { }
    }
}
