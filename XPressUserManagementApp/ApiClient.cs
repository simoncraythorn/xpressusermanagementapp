﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Configuration;

namespace XPressUserManagementApp
{
    /// <summary>
    /// base for the client 
    /// </summary>
    public class ApiClientBase
    {
        #region FIELDS

        /// <summary>
        /// Property that does the actual talking to the API
        /// </summary>
        private HttpActions _actions;

        /// <summary>
        /// Internal StopWatch for performance benchmarking and response timing.
        /// </summary>
        private Stopwatch _stopWatch;

        /// <summary>
        /// WebApi endpoint address
        /// </summary>
        private string _baseUrl;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Indicates whether or not to use the stopwatch to time web client calls
        /// </summary>
        public bool TimeResponses { get; set; }

        /// <summary>
        /// Returns the elapsed time of the last request made using the underlying webclient.
        /// This only gets set if TimeResponses = true.
        /// </summary>
        public long ElapsedTime { get { return _stopWatch.ElapsedMilliseconds; } }

        #endregion

        #region CONSTRUCTORS

        public ApiClientBase()
        {

            var baseUrl = "";
                
            
                // Get path from config file
            if (Properties.Settings.Default.BaseServiceUrlStg != null)
                baseUrl = Properties.Settings.Default.BaseServiceUrlStg;
                else
                {
                    throw new NullReferenceException("The AppSetting 'BaseServiceUrlStg' does not exist. You must provide an explicit path OR have a valid path set in your application config file.");
                }

            

            SetApiContext(null, baseUrl);
            InitTimer();
        }

        #endregion

        #region PRIVATE METHODS

        private void InitTimer()
        {
            _stopWatch = new Stopwatch();
        }

        /// <summary>
        /// Constructs an ApiException HttpResponseMessage
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private static ApiException CreateApiException(HttpResponseMessage response)
        {
            var httpErrorObject = response.Content.ReadAsStringAsync().Result;

            // Create an anonymous object to use as the template for deserialization:
            var anonymousErrorObject =
                new { StatusCode = "", message = "", ModelState = new Dictionary<string, string[]>() };

            // Deserialize:
            var deserializedErrorObject =
                JsonConvert.DeserializeAnonymousType(httpErrorObject, anonymousErrorObject);

            // Now wrap into an exception which best fullfills the needs of your application:
            var ex = (response.StatusCode == HttpStatusCode.BadRequest) ? new ApiBadRequestException(response) : new ApiException(response);

            // Sometimes, there may be Model Errors:
            if (deserializedErrorObject.ModelState != null)
            {
                var errors =
                    deserializedErrorObject.ModelState
                                            .Select(kvp => string.Join(". ", kvp.Value));

                var enumerable = errors as string[] ?? errors.ToArray();
                for (var i = 0; i < enumerable.Length; i++)
                {
                    // Wrap the errors up into the base Exception.Data Dictionary:
                    ex.Data.Add(i, enumerable.ElementAt(i));
                }

                ex.HasModelStateErrors = true;
            }
            // Othertimes, there may not be Model Errors:
            else
            {
                var error =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(httpErrorObject);
                foreach (var kvp in error)
                {
                    // Wrap the errors up into the base Exception.Data Dictionary:
                    ex.Data.Add(kvp.Key, kvp.Value);
                }
            }

            ex.ErrorMessage = deserializedErrorObject.message;
            return ex;
        }

        #endregion

        #region public METHODS

        /// <summary>
        /// Returns the WebApi endpoint address as a Uri
        /// </summary>
        public Uri GetBaseAddress()
        {
            return new Uri(_baseUrl);
        }

        public void SetBaseUrlForEnvironment(string environment)
        {
            switch (environment)
            {
                case "Staging":
                    _baseUrl = Properties.Settings.Default.BaseServiceUrlStg;
                    break;
                case "Onsite.Local":
                    _baseUrl = Properties.Settings.Default.BaseServiceUrlOnisteLocal;
                    break;
                case "Production":
                    _baseUrl = Properties.Settings.Default.BaseServiceUrlProd;
                    break;
            }

            _actions._baseAddress = _baseUrl;


        }

        /// <summary>
        /// Allows for generic use of the Get method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodPath"></param>
        /// <returns></returns>
        public T ApiGet<T>(string methodPath)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiGet(methodPath);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            // OLD WAY WITH SYSTEM.WEB.HTTPFORMATTING
            return response.Content.ReadAsAsync<T>().Result;

            // NEW WAY WITH JSON.NET - SHOULD WORK EXACTLY THE SAME
            //return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
        }

        /// <summary>
        /// Returns the Raw HttpResponseMessage from the supplied path. No conversion
        /// or casting of the content is applied. In short, you must know what type of data
        /// you are getting back from the call explicitly.
        /// </summary>
        /// <param name="methodPath"></param>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage ApiGetRaw(string methodPath)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiGet(methodPath);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response;
        }

        public T ApiPost<T>(string methodPath, T data)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiPost(methodPath, data);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsAsync<T>().Result;
        }

        public T2 ApiPost<T1, T2>(string methodPath, T1 data)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiPost(methodPath, data);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsAsync<T2>().Result;
        }

        public T1 ApiPostByteArray<T1>(string methodPath, Byte[] data)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiPostByteArray(methodPath, data);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsAsync<T1>().Result;
        }

        public T ApiPostFormUrlEncoded<T>(string methodPath, List<KeyValuePair<string, string>> values)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var request = new HttpRequestMessage(HttpMethod.Post, methodPath)
            {
                Content = new FormUrlEncodedContent(values)
            };

            var response = _actions.ApiPostHttpRequest(request);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsAsync<T>().Result;
        }

        public HttpResponseMessage ApiPostFormUrlEncodedRaw(string methodPath, List<KeyValuePair<string, string>> values)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var request = new HttpRequestMessage(HttpMethod.Post, methodPath)
            {
                Content = new FormUrlEncodedContent(values)
            };

            var response = _actions.ApiPostHttpRequest(request);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response;
        }

        public string ApiPostWithReturnString(string methodPath, object data)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiPost(methodPath, data);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsStringAsync().Result;
        }

        public T ApiPut<T>(string methodPath, FormCollection data)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiPut(methodPath, data);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsAsync<T>().Result;
        }

        public T ApiPut<T>(string methodPath, T data)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiPut(methodPath, data);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsAsync<T>().Result;
        }

        public T ApiDelete<T>(string methodPath)
        {
            if (TimeResponses)
            {
                _stopWatch.Restart();
            }

            var response = _actions.ApiDelete(methodPath);

            if (TimeResponses)
            {
                _stopWatch.Stop();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw CreateApiException(response);
            }

            return response.Content.ReadAsAsync<T>().Result;
        }

        /// <summary>
        /// Set runtime context for the Api 
        /// </summary>
        /// <param name="authorizationToken" type="string"></param>
        /// <param name="baseUrl" type="string"></param>
        public void SetApiContext(string authorizationToken, string baseUrl)
        {
            _baseUrl = baseUrl;
            _actions = new HttpActions(authorizationToken, baseUrl);
        }

        #endregion

        #region PUBLIC METHODS

        public void SetAuthorizationToken(string authorizationToken)
        {
            _actions.SetAuthorizationToken(authorizationToken);
        }

        /// <summary>
        /// Receives an ApiException (or ApiBadRequestException which inherits from ApiException) and extracts the
        /// error massages from the Errors collection and adds them to the supplied ModelState dictionary.
        /// </summary>
        /// <param name="ex" type="ApiException">ApiException or direct descendant</param>
        /// <param name="modelState" type="ModelStateDictionary">ModelState dictionary</param>
        public void AddErrorsToModelState(ApiException ex, ModelStateDictionary modelState)
        {
            if (ex == null || ex.Errors == null || !ex.Errors.Any()) return;

            foreach (var apiError in ex.Errors)
            {
                modelState.AddModelError("", apiError);
            }
        }

        #endregion
    }
}
