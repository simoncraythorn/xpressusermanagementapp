﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web;

namespace XPressUserManagementApp
{
    [DataContract]
    public class ApiException : HttpException
    {
        #region CONSTRUCTORS

        public ApiException(HttpResponseMessage response)
        {
            Response = response;
        }

        #endregion

        [DataMember]
        public Guid ErrorLogId { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public HttpResponseMessage Response { get; set; }

        [DataMember]
        public HttpStatusCode StatusCode
        {
            get
            {
                return Response.StatusCode;
            }
        }

        [DataMember]
        public string ReasonPhrase
        {
            get { return Response.ReasonPhrase; }
        }

        [DataMember]
        public string Content
        {
            get { return Response.Content.ToString(); }
        }

        [DataMember]
        public bool HasModelStateErrors { get; set; }

        [DataMember]
        public IEnumerable<string> Errors
        {
            get
            {
                return Data.Values.Cast<string>().ToList();
            }
        }
    }
}
