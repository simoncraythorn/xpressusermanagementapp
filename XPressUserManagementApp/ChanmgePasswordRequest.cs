﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPressUserManagementApp
{
    public class ChangePasswordRequest
    {
        public string adminUsername { get; set; }
        public string adminPassword { get; set; }
        public string username { get; set; }
        public string newPassword { get; set; }

        /// <summary>
        /// Default 
        /// </summary>
        public ChangePasswordRequest()
        {

        }
    }
}
