﻿namespace XPressUserManagementApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtAdminUsername = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbEnvironment = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTab1PasswordAppendix = new System.Windows.Forms.TextBox();
            this.btnTab1SaveCSV = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTab1Results = new System.Windows.Forms.TextBox();
            this.lstUsers = new System.Windows.Forms.ListBox();
            this.txtTab1Search = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkTab1CheckAll = new System.Windows.Forms.CheckBox();
            this.btnTab1Execute = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAdminPassword = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.txtFTab1ailures = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name:";
            // 
            // txtAdminUsername
            // 
            this.txtAdminUsername.Location = new System.Drawing.Point(78, 6);
            this.txtAdminUsername.Name = "txtAdminUsername";
            this.txtAdminUsername.Size = new System.Drawing.Size(191, 20);
            this.txtAdminUsername.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-5, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(799, 459);
            this.tabControl1.TabIndex = 200;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtNewPassword);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtFTab1ailures);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.cmbEnvironment);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtTab1PasswordAppendix);
            this.tabPage1.Controls.Add(this.btnTab1SaveCSV);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtTab1Results);
            this.tabPage1.Controls.Add(this.lstUsers);
            this.tabPage1.Controls.Add(this.txtTab1Search);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.chkTab1CheckAll);
            this.tabPage1.Controls.Add(this.btnTab1Execute);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(791, 433);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Bulk Password Change";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 65;
            this.label6.Text = "Environment:";
            // 
            // cmbEnvironment
            // 
            this.cmbEnvironment.FormattingEnabled = true;
            this.cmbEnvironment.Items.AddRange(new object[] {
            "Staging",
            "Onsite.Local",
            "Production"});
            this.cmbEnvironment.Location = new System.Drawing.Point(76, 93);
            this.cmbEnvironment.Name = "cmbEnvironment";
            this.cmbEnvironment.Size = new System.Drawing.Size(216, 21);
            this.cmbEnvironment.TabIndex = 64;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 63;
            this.label5.Text = "Pass End:";
            // 
            // txtTab1PasswordAppendix
            // 
            this.txtTab1PasswordAppendix.Location = new System.Drawing.Point(76, 46);
            this.txtTab1PasswordAppendix.Name = "txtTab1PasswordAppendix";
            this.txtTab1PasswordAppendix.Size = new System.Drawing.Size(216, 20);
            this.txtTab1PasswordAppendix.TabIndex = 62;
            // 
            // btnTab1SaveCSV
            // 
            this.btnTab1SaveCSV.Location = new System.Drawing.Point(678, 13);
            this.btnTab1SaveCSV.Name = "btnTab1SaveCSV";
            this.btnTab1SaveCSV.Size = new System.Drawing.Size(103, 23);
            this.btnTab1SaveCSV.TabIndex = 61;
            this.btnTab1SaveCSV.Text = "Save As CSV";
            this.btnTab1SaveCSV.UseVisualStyleBackColor = true;
            this.btnTab1SaveCSV.Click += new System.EventHandler(this.btnTab1SaveCSV_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(304, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Successes:";
            // 
            // txtTab1Results
            // 
            this.txtTab1Results.Location = new System.Drawing.Point(304, 39);
            this.txtTab1Results.Multiline = true;
            this.txtTab1Results.Name = "txtTab1Results";
            this.txtTab1Results.Size = new System.Drawing.Size(481, 207);
            this.txtTab1Results.TabIndex = 7;
            this.txtTab1Results.TabStop = false;
            // 
            // lstUsers
            // 
            this.lstUsers.FormattingEnabled = true;
            this.lstUsers.Location = new System.Drawing.Point(6, 142);
            this.lstUsers.Name = "lstUsers";
            this.lstUsers.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstUsers.Size = new System.Drawing.Size(286, 277);
            this.lstUsers.TabIndex = 60;
            // 
            // txtTab1Search
            // 
            this.txtTab1Search.Location = new System.Drawing.Point(76, 19);
            this.txtTab1Search.Name = "txtTab1Search";
            this.txtTab1Search.Size = new System.Drawing.Size(216, 20);
            this.txtTab1Search.TabIndex = 30;
            this.txtTab1Search.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Search:";
            // 
            // chkTab1CheckAll
            // 
            this.chkTab1CheckAll.AutoSize = true;
            this.chkTab1CheckAll.Location = new System.Drawing.Point(6, 122);
            this.chkTab1CheckAll.Name = "chkTab1CheckAll";
            this.chkTab1CheckAll.Size = new System.Drawing.Size(70, 17);
            this.chkTab1CheckAll.TabIndex = 50;
            this.chkTab1CheckAll.Text = "Select All";
            this.chkTab1CheckAll.UseVisualStyleBackColor = true;
            this.chkTab1CheckAll.CheckedChanged += new System.EventHandler(this.chkTab1CheckAll_CheckedChanged);
            // 
            // btnTab1Execute
            // 
            this.btnTab1Execute.Location = new System.Drawing.Point(166, 117);
            this.btnTab1Execute.Name = "btnTab1Execute";
            this.btnTab1Execute.Size = new System.Drawing.Size(126, 23);
            this.btnTab1Execute.TabIndex = 40;
            this.btnTab1Execute.Text = "Change Passwords";
            this.btnTab1Execute.UseVisualStyleBackColor = true;
            this.btnTab1Execute.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnTab1Execute_MouseClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(791, 433);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "TBD";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password:";
            // 
            // txtAdminPassword
            // 
            this.txtAdminPassword.Location = new System.Drawing.Point(335, 6);
            this.txtAdminPassword.Name = "txtAdminPassword";
            this.txtAdminPassword.PasswordChar = '*';
            this.txtAdminPassword.Size = new System.Drawing.Size(200, 20);
            this.txtAdminPassword.TabIndex = 20;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "PasswordChange.csv";
            // 
            // txtFTab1ailures
            // 
            this.txtFTab1ailures.Location = new System.Drawing.Point(304, 269);
            this.txtFTab1ailures.Multiline = true;
            this.txtFTab1ailures.Name = "txtFTab1ailures";
            this.txtFTab1ailures.Size = new System.Drawing.Size(477, 150);
            this.txtFTab1ailures.TabIndex = 66;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(308, 253);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "Failures:";
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Location = new System.Drawing.Point(76, 68);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.Size = new System.Drawing.Size(216, 20);
            this.txtNewPassword.TabIndex = 68;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 69;
            this.label8.Text = "or New Pass:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 508);
            this.Controls.Add(this.txtAdminPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAdminUsername);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "X•Press Reg Member Services Management Console";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAdminUsername;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAdminPassword;
        private System.Windows.Forms.Button btnTab1Execute;
        private System.Windows.Forms.CheckBox chkTab1CheckAll;
        private System.Windows.Forms.TextBox txtTab1Search;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstUsers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTab1Results;
        private System.Windows.Forms.Button btnTab1SaveCSV;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTab1PasswordAppendix;
        private System.Windows.Forms.ComboBox cmbEnvironment;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFTab1ailures;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.Label label8;
    }
}

