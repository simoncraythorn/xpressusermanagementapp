﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XPressUserManagementApp
{
    public partial class Form1 : Form
    {
        private ApiClientBase _api { get; set; }
        private string runningCSVContent { get; set; }
        private string runningFailues { get; set; }
        
        public Form1()
        {
            InitializeComponent();
            Init();
        }


        private void Init()
        {
            this._api =  new ApiClientBase();
            BindUserList();

        }

        /// <summary>
        /// Fires the *pinigng* to change the user name/password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTab1Execute_MouseClick(object sender, MouseEventArgs e)
        {

            
            runningCSVContent = "";//clear this out
            runningFailues = "";
            var Environment = cmbEnvironment.Text;
           this._api.SetBaseUrlForEnvironment(Environment);
            
            
            if (lstUsers.SelectedItems.Count == 0)
            {
                MessageBox.Show("No selections were made", "No Selection");
            }
            else
            {
                var result = MessageBox.Show("Are you sure?");

                if(!string.IsNullOrWhiteSpace(txtNewPassword.Text) && lstUsers.SelectedItems.Count > 1)
                {
                    MessageBox.Show("NYou have too many users selected for the custom password to be used", "");
                    
                }else
                {

                    if (result == DialogResult.OK)
                    {
                        foreach (var thisItem in lstUsers.SelectedItems)
                        {
                            //SendUser(thisItem);
                            var UserID = ((User_Profiles) thisItem).UserID;
                            var UserName = ((User_Profiles) thisItem).UserName;
                            var FirstName = ((User_Profiles) thisItem).FirstName;
                            var LastName = ((User_Profiles) thisItem).LastName;
                            var Email = ((User_Profiles) thisItem).Email;

                            try
                            {
                                SendUser(UserName, UserID, FirstName, LastName, Email);
                            }
                            catch (Exception ex)
                            {
                                runningFailues += string.Format("{0} filed {1}", UserName, ex.Message);
                            }

                        }
                    }
                }

            }

            //export the users?
            txtTab1Results.Text = runningCSVContent;
            txtFTab1ailures.Text = runningFailues;

        }

     
        /// <summary>
        /// Used to send the user to the user to the URL
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userID"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        private void SendUser(string userName, int userID, string firstname, string lastname, string email)
        {
            var request = new ChangePasswordRequest();
            var appendix = txtTab1PasswordAppendix.Text;
            var newPassword = txtNewPassword.Text;

            if (string.IsNullOrWhiteSpace(appendix) && string.IsNullOrWhiteSpace(newPassword))
            {
                MessageBox.Show("Please enter a password appendix or new password");
            }
            else
            {


                request.adminPassword = txtAdminPassword.Text;
                request.adminUsername = txtAdminUsername.Text;
                request.username = userName;

                if (!string.IsNullOrWhiteSpace(newPassword))
                {
                    request.newPassword = newPassword;
                }
                else
                {
                    request.newPassword = userName + userID.ToString() + appendix;
                }

                if (request.adminUsername.ToLower() == userName.ToLower())
                {
                    runningFailues += String.Format("{0} not changed, same as admin user\r\n", userName);
                }
                else
                {


                    if (_api.ApiPost<ChangePasswordRequest, bool>("ManageUser/ChangePasswordAdmin/", request))
                    {
                        runningCSVContent += String.Format("{0},{1},{2},{3},{4}\r\n", userName, request.newPassword,
                            firstname, lastname, email);
                    }
                    else
                    {
                        runningFailues += String.Format("{0} failed\r\n", userName);
                    }
                }


            }


        }

        /// <summary>
        /// use to actuallty search and bind the user names to the list box
        /// </summary>
        /// <param name="SearchString"></param>
        private void BindUserList(string SearchString = "")
        {
            var XrmsContext = new XpressRegMemberServicesEntities();
            var XrpContext =  new XpressRegPortalEntities();

            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                //lstUsers.DataSource = XrmsContext.aspnet_Users.Where(m=>m.UserName.Contains(SearchString)).OrderBy(m => m.UserName).ToList();
                lstUsers.DataSource =
                    XrpContext.User_Profiles.Where(m => m.UserName.Contains(SearchString))
                        .OrderBy(m => m.UserName)
                        .ToList();
            }
            else
            {
                //lstUsers.DataSource = XrmsContext.aspnet_Users.OrderBy(m => m.UserName).ToList();  
                lstUsers.DataSource =
                   XrpContext.User_Profiles.OrderBy(m => m.UserName)
                       .ToList();
            }


            lstUsers.DisplayMember = "UserName";
            lstUsers.ValueMember = "UserID";


        }

        /// <summary>
        /// Will fill as you type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var SearchString = txtTab1Search.Text;
            BindUserList(SearchString);
        }

        private void btnTab1SaveCSV_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // create a writer and open the file
                TextWriter tw = new StreamWriter(saveFileDialog1.FileName);
                // write a line of text to the file
                tw.WriteLine(txtTab1Results.Text);
                // close the stream
                tw.Close();
                MessageBox.Show("Saved to " + saveFileDialog1.FileName, "Saved Log File", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Use to *select all*
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkTab1CheckAll_CheckedChanged(object sender, EventArgs e)
        {
            var isSelected= chkTab1CheckAll.Checked;

            for (int i = 0; i < lstUsers.Items.Count; i++)
            {
                lstUsers.SetSelected(i, isSelected);
            }
        }
    }
}
