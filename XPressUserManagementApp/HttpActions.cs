﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace XPressUserManagementApp
{
    /// <summary>
    /// Wrapper for HttpClient verbs  
    /// </summary>
    public class HttpActions
    {
        #region FIELDS

        private string _authorizationToken;
        public string _baseAddress;

        #endregion

        #region CONSTRUCTORS

        public HttpActions(string authorizationToken, string baseUrl)
        {
            _authorizationToken = authorizationToken;
            _baseAddress = baseUrl;
        }

        #endregion

        #region PRIVATE METHODS

        private HttpClient GetConfiguredHttpClient()
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (_authorizationToken != null)
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", _authorizationToken));

            client.BaseAddress = new Uri(_baseAddress);

            return client;
        }

        #endregion

        #region INTERNAL METHODS

        /// <summary>
        /// Authorization token for account login pass-through to HttpClient
        /// </summary>
        /// <param name="authorizationToken"></param>
        internal void SetAuthorizationToken(string authorizationToken)
        {
            _authorizationToken = authorizationToken;
        }

        /// <summary>
        /// Configures HttpClient and calls GetAsync
        /// </summary>
        /// <param name="methodPath" type="string"></param>
        /// <returns>HttpResponseMessage</returns>
        internal HttpResponseMessage ApiGet(string methodPath)
        {
            using (var client = GetConfiguredHttpClient())
            {
                return client.GetAsync(methodPath, HttpCompletionOption.ResponseContentRead).Result;
            }
        }

        /// <summary>
        /// Configures HttpClient and calls PostAsync
        /// </summary>
        /// <param name="methodPath" type="string"></param>
        /// <param name="data" type="T"></param>
        /// <returns>HttpResponseMessage</returns>
        internal HttpResponseMessage ApiPost<T>(string methodPath, T data)
        {
            using (var client = GetConfiguredHttpClient())
            {
                return client.PostAsync(methodPath, data, new JsonMediaTypeFormatter()).Result;
            }
        }

        /// <summary>
        /// Configures HttpClient and calls PostAsync
        /// </summary>
        /// <param name="methodPath" type="string"></param>
        /// <param name="data" type="T"></param>
        /// <returns>HttpResponseMessage</returns>
        internal HttpResponseMessage ApiPostByteArray(string methodPath, Byte[] data)
        {
            using (var client = GetConfiguredHttpClient())
            {
                return client.PostAsync(methodPath, new ByteArrayContent(data)).Result;
            }
        }

        /// <summary>
        /// Configures HttpClient and calls SendAsync
        /// </summary>
        /// <param name="request" type="HttpRequestMessage"></param>
        /// <returns>HttpResponseMessage</returns>
        internal HttpResponseMessage ApiPostHttpRequest(HttpRequestMessage request)
        {
            using (var client = GetConfiguredHttpClient())
            {
                return client.SendAsync(request).Result;
            }
        }

        /// <summary>
        /// Configures HttpClient and calls PutAsync
        /// </summary>
        /// <param name="methodPath" type="string"></param>
        /// <param name="data" type="FormCollection"></param>
        /// <returns>HttpResponseMessage</returns>
        internal HttpResponseMessage ApiPut(string methodPath, FormCollection data)
        {
            using (var client = GetConfiguredHttpClient())
            {
                return client.PutAsync(methodPath, data, new JsonMediaTypeFormatter()).Result;
            }
        }

        /// <summary>
        /// Configures HttpClient and calls PutAsync
        /// </summary>
        /// <param name="methodPath" type="string"></param>
        /// <param name="data" type="T"></param>
        /// <returns>HttpResponseMessage</returns>
        internal HttpResponseMessage ApiPut<T>(string methodPath, T data)
        {
            using (var client = GetConfiguredHttpClient())
            {
                return client.PutAsync(methodPath, data, new JsonMediaTypeFormatter()).Result;
            }
        }

        /// <summary>
        /// Configures HttpClient and calls DeleteAsync
        /// </summary>
        /// <param name="methodPath" type="string"></param>
        /// <returns>HttpResponseMessage</returns>
        internal HttpResponseMessage ApiDelete(string methodPath)
        {
            using (var client = GetConfiguredHttpClient())
            {
                return client.DeleteAsync(methodPath).Result;
            }
        }

        #endregion
    }
}
